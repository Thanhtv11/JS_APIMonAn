export const turnSpiner = {
    turnOn: () =>{
        document.getElementById("loading").style.display = "flex";
        document.getElementById("content").style.display = "none";
    },

    turnOff: () =>{
        document.getElementById("loading").style.display = "none";
        document.getElementById("content").style.display = "block";
    },

}