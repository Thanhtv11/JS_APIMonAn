let foodList = [];
let idFood = null;


let xoaItem = (maMonAn) => {
    turnSpiner.turnOn();
    monAnService.xoaMonAn(maMonAn)
    .then((res)=>{
        turnSpiner.turnOff();
        renderDanhSachService()
    })
    .catch((err)=>{
        turnSpiner.turnOff();
    })
}

window.xoaItem = xoaItem

let renderTable = (list) => {
    let contentHTML = "";
    for(let i = 0; i < list.length; i++){
        let monAn = list[i]
        let contentLayout = `<tr>
                          <td>${monAn.id}</td>
                          <td>${monAn.foodname}</td>
                          <td>${monAn.price}</td>
                          <td>${monAn.description}</td>
                         
                          <td><button onclick="layChiTietMonAn(${monAn.id})" class="btn btn-success">Sửa</button></td>
                          <td><button onclick="xoaItem(${monAn.id})" class="btn btn-primary">Xóa</button></td>
                          
                        </tr>`
        contentHTML += contentLayout
    } document.getElementById("tbody_food").innerHTML = contentHTML;

} 

let renderDanhSachService = ()=>{  
    monAnService.layDanhSachMonAn()
    .then((res) => {
        turnSpiner.turnOff();
        foodList = res.data;
        renderTable(foodList)
    })
    .catch((err) => {
        turnSpiner.turnOff(); 
    })
};

let layChiTietMonAn = (idMonAn) =>{
    idFood = idMonAn;
    turnSpiner.turnOn(); 
    monAnService.layThongTinChiTiet(idFood)
    .then((res)=>{
        document.getElementById("tenmon").value = (res.data).foodname
        document.getElementById("giamon").value = (res.data).price
        document.getElementById("mota").value = (res.data).description
        turnSpiner.turnOff(); 
    })
    .catch((err)=>{
        turnSpiner.turnOff(); 
    })
}

window.layChiTietMonAn = layChiTietMonAn

let capNhatMonAn = () => {
    turnSpiner.turnOn();
    let monAn = monAnController.layThongTinTuForm();
    let newItem = {...monAn, id: idFood}    
    monAnService.capNhatMonAn(newItem)
    .then((res)=>{
        renderDanhSachService()
        monAnController.hienInputRong()
        console.log(res);
    })
    .catch((err)=>{
        console.log(err);
    })
}
window.capNhatMonAn = capNhatMonAn

let addItem = () => {
    turnSpiner.turnOn();   
    let monAn = monAnController.layThongTinTuForm()
    monAnService.themMoiMonAn(monAn)
    .then((res)=>{  
        renderDanhSachService()
    })
    .catch((err)=>{
        console.log("err",err);
    })
    turnSpiner.turnOff()  
}
window.addItem = addItem   







import  { monAnService } from "./monAn.js"
import { turnSpiner } from "./spiner.js";
import { monAnController } from "./monancontroller.js"
turnSpiner.turnOn();
monAnService.layDanhSachMonAn().then((res) => {
    turnSpiner.turnOff();
    foodList = res.data;
    renderTable(foodList)
})
.catch((err) => {
    turnSpiner.turnOff();
})
