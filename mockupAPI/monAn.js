// var BASE_URL = `https://62b07878e460b79df0469b82.mockapi.io/mon-an`

export let monAnService = {
    layDanhSachMonAn: () => {
        return axios({
            url:"https://62b07878e460b79df0469b82.mockapi.io/mon-an",
            method: "GET",
        })
    },

    xoaMonAn:(maMonAn) => {
        return axios({
            url:`https://62b07878e460b79df0469b82.mockapi.io/mon-an/${maMonAn}`,
            method: "DELETE",
        })
    },
    themMoiMonAn: (monAn) => {
        return axios({
            url:"https://62b07878e460b79df0469b82.mockapi.io/mon-an",
            method: "POST",
            data: monAn,
        })
    },

    layThongTinChiTiet: (idMonAn) =>{
        return axios({
            url:`https://62b07878e460b79df0469b82.mockapi.io/mon-an/${idMonAn}`,
            method: "GET",
            data:idMonAn,
        })
    },

    capNhatMonAn: (foodItem) =>{
        return axios({
            url: `https://62b07878e460b79df0469b82.mockapi.io/mon-an/${foodItem.id}`,
            method: "PUT",
            data: foodItem,
        })
    },

};

