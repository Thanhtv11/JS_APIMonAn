export let monAnController = {
    layThongTinTuForm: ()=>{
        let tenMon = document.getElementById("tenmon").value;
        let giaMon = document.getElementById("giamon").value;
        let moTa = document.getElementById("mota").value;
        let monAn =  {
            name:tenMon,
            price:giaMon,
            description:moTa,
        };
        return monAn;
    },

    hienInputRong: () =>{
        document.getElementById("tenmon").value = '';
        document.getElementById("giamon").value = '';
        document.getElementById("mota").value = '';
    }
}